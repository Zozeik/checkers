#include <iostream>
#include "Poco/Net/TCPServer.h"
#include "Poco/Net/TCPServerParams.h"
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/Net/Socket.h"
#include <nlohmann/json.hpp>
#include <queue>
#include <thread>

using namespace std;

class Client{
private:
    std::string id;
    Poco::Net::StreamSocket *Socket;
    bool searchGame = false;
    bool isGaming = false;
    bool isBlocking = false;
public:



    std::string GetID(){
        return this->id;
    }

    void SetStreamSocket(Poco::Net::StreamSocket &ss){
        try {
            this->Socket = &ss;
        }
        catch (Poco::Exception exception){
           std::cout << exception.message() <<std::flush;
        }

    }

    void SetID(std::string id){
        this->id = id;
    }

    bool GetSearchStatus(){
       return this->searchGame ;
    }

    void SetSearchStatus(bool status){
        this->searchGame = status;
    }

    bool GetGameStatus(){
        return this->isGaming;
    }

    void SetGamingStatus(bool status){
        this->isGaming = status;
    }

    bool GetBlockingStatus(){
        return this->isBlocking;
    }

    void SetBlockingStatus(bool block){
        this->isBlocking = block;
    }

    void GameIsCreated(std::string id, std::string colorCheckers){
        nlohmann::json  json;
        json["from"] = "Client";
        json["clientID"] = this->GetID();
        json["message"] = "game_is_start";
        json["opponentID"] = id;
        json["clr"] = colorCheckers;
        this->SendData(json.dump());
    }

    void Motion(std::string id,nlohmann::json command){
        nlohmann::json  json;
        json["from"] = "Client";
        json["clientID"] = this->GetID();
        json["message"] = "game";
        json["opponentID"] = id;
        json["motion"] = command;
        this->SendData(json.dump());
    }

    void SendData(std::string message){
        Socket->sendBytes(message.data(),message.length()+1);
    }

};

std::map<std::string,Client> clients;

class Manager{
private:
    bool isWork = true;
public:
    void StartGameCreator(){
        std::thread creator(&Manager::TaskCreator,this);
        creator.detach();
    }

    void TaskCreator(){
       while(isWork){
            std::vector<Client> game;
            if(!clients.empty()) {
                for (auto it : clients) {
                    if (game.size() == 2) {
                        break;
                    }
                    if (it.second.GetSearchStatus() == true) {
                        game.push_back(it.second);
                    }
                }
                if (game.size() == 2) {
                    clients[game[0].GetID()].SetSearchStatus(false);
                    clients[game[1].GetID()].SetSearchStatus(false);

                    clients[game[0].GetID()].GameIsCreated(game[1].GetID(), "white");
                    clients[game[1].GetID()].GameIsCreated(game[0].GetID(), "black");
                }
            }
        }
    }

};


class newConnection: public Poco::Net::TCPServerConnection {
private:
    Client *client;
public:
    newConnection(const Poco::Net::StreamSocket& s) :
            Poco::Net::TCPServerConnection(s) {
    }

    void run() {

        cout << "New connection from: " << socket().peerAddress().host().toString() <<  endl << flush;
        bool isOpen = true;
        Poco::Timespan timeOut(10,0);
        char incommingBuffer[1000];
        while(isOpen){
            if (socket().poll(timeOut,Poco::Net::Socket::SELECT_READ) == false){
                cout << "TIMEOUT!" << endl << flush;
            }
            else{
                cout << "RX EVENT!!! ---> "   << flush;
                int nBytes = -1;

                try {
                    nBytes = socket().receiveBytes(incommingBuffer, sizeof(incommingBuffer));
                }
                catch (Poco::Exception& exc) {
                    //Handle your network errors.
                    cerr << "Network error: " << exc.displayText() << endl;
                    isOpen = false;
                }


                if (nBytes==0){
                    cout << "Client closes connection!" << endl << flush;
                    isOpen = false;
                    std::map<std::string,Client>::iterator it = clients.find(this->client->GetID());
                    clients.erase(it);
                }
                else{
                    std::string request(incommingBuffer);
                    nlohmann::json json = nlohmann::json::parse(request);
                    if(json["from"] == "server"){
                        if(json["request"]=="auth"){
                            this->HelloClient(json);
                            this->client = new Client();
                            this->client->SetStreamSocket(this->socket());
                            this->client->SetID(json["client"]["id"]);
                            std::cout << "Client : " << json["client"]["id"] << std::flush;
                            clients[json["client"]["id"]] = *client;
                        }
                        if(json["request"] == "search_game"){
                            std::string str = json.dump();
                            clients[json["id"]].SetSearchStatus(true);
                        }
                        if(json["request"] == "game"){
                            std::string opponentID = json["opponentID"];
                            clients[opponentID].Motion(json["id"],json["motion"]);
                        }

                    }
                }
            }
        }
        cout << "Connection finished!" << endl << flush;
    }

    void HelloClient(nlohmann::json json){
        nlohmann::json clientInfo = json["client"];
        nlohmann::json jsonResponse;
        jsonResponse["from"] = "Client";
        jsonResponse["clientID"] = clientInfo["id"];
        jsonResponse["message"] = "Hello";
        std::string response = jsonResponse.dump();
        socket().sendBytes(response.data(),response.length()+1);
    }

    void SearchGame(){
        client->SetSearchStatus(true);
    }

};

int main(int argc, char** argv) {

    //Create a server socket to listen.
    Poco::Net::ServerSocket svs(1234);

    //Configure some server params.
    Poco::Net::TCPServerParams* pParams = new Poco::Net::TCPServerParams();
    pParams->setMaxThreads(4);
    pParams->setMaxQueued(4);
    pParams->setThreadIdleTime(100);

    //Create your server
    Poco::Net::TCPServer myServer(new Poco::Net::TCPServerConnectionFactoryImpl<newConnection>(), svs, pParams);
    myServer.start();
    Manager manager;
    manager.StartGameCreator();
    while(true);

    return 0;
}
