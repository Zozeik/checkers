#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <vector>
#include "Poco/Net/TCPServerParams.h"
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/Net/Socket.h"
#include <thread>
#include <nlohmann/json.hpp>
#include <queue>
//Расставляем белые шашки по своим позициям
//Идём подряд по 3 линиями
void SetWhiteInitPosition(std::vector<sf::CircleShape > &shapes,int x,int y ,int margin){
    x = margin;
    int step = 0;
    for(int i = 0;i<4;i++){
        shapes[i].setRadius(28);
        shapes[i].setPosition(x + margin*step,y);
        shapes[i].setFillColor(sf::Color::White);
        shapes[i].setOutlineThickness(3);
        x += margin;
        step++;
    }
    x = 0;
    y = margin;
    step = 0;
    for(int i = 4; i<8; i++){
        shapes[i].setRadius(28);
        shapes[i].setPosition(x + margin*step,y);
        shapes[i].setFillColor(sf::Color::White);
        shapes[i].setOutlineThickness(3);
        x += margin;
        step++;
    }
    x = margin;
    y+=margin;
    step = 0;
    for(int i = 8; i<12; i++){
        shapes[i].setRadius(28);
        shapes[i].setPosition(x + margin*step,y);
        shapes[i].setFillColor(sf::Color::White);
        shapes[i].setOutlineThickness(3);
        x += margin;
        step++;
    }
}
//Расставляем чёрные шашки по позициям
//Аналогично методу выше
void SetBlackInitPosition(std::vector<sf::CircleShape > &shapes,int x,int y ,int margin){
    int step = 0;
    x = 0;
    y = margin * 7;

    for(int i = 0; i < 4; i++){
        shapes[i].setRadius(28);
        shapes[i].setPosition(x + step*margin,y);
        shapes[i].setFillColor(sf::Color::Yellow);
        shapes[i].setOutlineThickness(3);
        shapes[i].setOutlineColor(sf::Color::Yellow);
        step++;
        x += margin;
    }

    x = margin;
    y = margin*6;
    step = 0;

    for(int i = 4; i < 8; i++){
        shapes[i].setRadius(28);
        shapes[i].setPosition(x + step*margin,y);
        shapes[i].setFillColor(sf::Color::Yellow);
        shapes[i].setOutlineThickness(3);
        shapes[i].setOutlineColor(sf::Color::Yellow);
        step++;
        x += margin;
    }

    step = 0;
    x = 0;
    y = margin * 5;

    for(int i = 8; i < 12; i++){
        shapes[i].setRadius(28);
        shapes[i].setPosition(x + step*margin,y);
        shapes[i].setFillColor(sf::Color::Yellow);
        shapes[i].setOutlineThickness(3);
        shapes[i].setOutlineColor(sf::Color::Yellow);
        step++;
        x += margin;
    }
}
//Рисуем доску
void DrawBoard(sf::RenderWindow & window,std::vector<std::vector<sf::RectangleShape>> & cells,int cellSize){
    sf::Vector2f vector(cellSize,cellSize);
    for(int i =0; i < cells.size();i ++){
        sf::Color color;
        if( i % 2 == 0){
            color = sf::Color::White;
        }
        else{
            color = sf::Color::Black;
        }
        for(int j = 0; j < cells[i].size();j++){
            cells[i][j].setPosition(cellSize*j  ,cellSize*i);
            cells[i][j].setFillColor(color);
            cells[i][j].setOutlineColor(color);
            cells[i][j].setOutlineThickness(3);
            cells[i][j].setSize(vector);
            window.draw(cells[i][j]);
            if(color == sf::Color::White)
                color = sf::Color::Black;
            else
                color = sf::Color::White;
        }
    }

}
//Рисуем фигуры на экране
void DrawFigureOnBoard(sf::RenderWindow & window, std::vector<sf::CircleShape > &shapes)
{
    for(auto i : shapes){
        window.draw(i);
    }
}
//Задаём или снимаем выделение с фигуры
std::pair<bool,int> SetOutlineColorForCircleShapes(std::vector<sf::CircleShape> &Shapes,int x,int y, int sizeCells){
    bool result= false;
    int curentShapeID = -1;

    for(int i = 0; i < Shapes.size(); i++){
        Shapes[i].setOutlineColor(Shapes[i].getFillColor());
        sf::Vector2f position = Shapes[i].getPosition();
        if(x >= position.x && x <=position.x + sizeCells && y >= position.y && y<= position.y + sizeCells){
            curentShapeID = i;
            result = true;
        }
    }
    return std::make_pair(result,curentShapeID);
}

sf::Vector2f GetPositionRectagleShape(std::vector<std::vector<sf::RectangleShape>> &Board,int x,int y,int cellSize){
    for(int i = 0; i < Board.size();i++){
        for(int j = 0; j < Board[i].size(); j ++){
            sf::Vector2f position = Board[i][j].getPosition();
            if(position.x <= x && x <= position.x + cellSize && position.y <= y && y <= position.y + cellSize){
                return position;
            }
        }
    }
}

bool CanMove(std::vector<sf::CircleShape> &Shapes,sf::Vector2f position){
    for(int i = 0; i < Shapes.size(); i++){
        if(Shapes[i].getPosition() == position){
            return false;
        }
    }
    return true;
}

bool CanMovedFigure(std::vector<std::vector<sf::RectangleShape>> &Board, sf::Vector2f position){
    for(int i = 0 ; i < Board.size(); i++){
        for(int j = 0; j<Board[i].size(); j++){
            if(Board[i][j].getPosition() == position  && Board[i][j].getFillColor() == sf::Color::Green){
               return true;
            }
        }
    }
    return false;
}

void DeleteFigureFromBoard(std::vector<sf::CircleShape> &whiteShapes, sf::Vector2f position){
    int id = -1;
    for(int i =0; i <whiteShapes.size(); i++){
        if(whiteShapes[i].getPosition() == position){
            id = i;
            break;
        }
    }
    if(id != -1) {
        whiteShapes.erase(whiteShapes.cbegin() + id);
    }
}

void SetFillColorForCells(sf::RenderWindow &window,
        std::vector<std::vector<sf::RectangleShape>> &Board,
        std::vector<sf::CircleShape> &whiteShapes,
        std::vector<sf::CircleShape> &blackShapes,
        sf::Color color,sf::Vector2f position,int cellSize){
    int i = position.x / cellSize;
    int j = position.y / cellSize;
    bool canMoveForward = true;

    if(sf::Color::White == color){
        if(i+2 <=7 && j+2 <=7){
            if(CanMove(blackShapes,Board[j+2][i+2].getPosition()) //проверяем не стоит ли на этой клетке чёрная шашка
            && CanMove(whiteShapes,Board[j+2][i+2].getPosition())//проверяем не стоит ли на этой клетке белая шашка
            && CanMove(blackShapes,Board[j+1][i+1].getPosition()) == false)//проверяем можем ли "перепрыгнуть" шашку противника
            {
                canMoveForward = false;
                Board[j+2][i+2].setFillColor(sf::Color::Green);
                window.draw(Board[j+2][i+2]);
            }
        }
        if(i -2 >= 0 && j+2 <=7){
            if(CanMove(blackShapes,Board[j+2][i-2].getPosition()) //проверяем не стоит ли на этой клетке чёрная шашка
               && CanMove(whiteShapes,Board[j+2][i-2].getPosition())//проверяем не стоит ли на этой клетке белая шашка
               && CanMove(blackShapes,Board[j+1][i-1].getPosition()) == false)//проверяем можем ли "перепрыгнуть" шашку противника
            {
                canMoveForward = false;
                Board[j+2][i-2].setFillColor(sf::Color::Green);
                window.draw(Board[j+2][i-2]);

            }
        }
        if(i -2 >=0 && j-2 >= 0 ){
            if(CanMove(blackShapes,Board[j-2][i-2].getPosition()) //проверяем не стоит ли на этой клетке чёрная шашка
               && CanMove(whiteShapes,Board[j-2][i-2].getPosition())//проверяем не стоит ли на этой клетке белая шашка
               && CanMove(blackShapes,Board[j-1][i-1].getPosition()) == false)//проверяем можем ли "перепрыгнуть" шашку противника
            {
                canMoveForward = false;
                Board[j-2][i-2].setFillColor(sf::Color::Green);
                window.draw(Board[j-2][i-2]);
            }
        }
        if(i + 2 <=7 && j-2 >=0){
            if(CanMove(blackShapes,Board[j-2][i+2].getPosition()) //проверяем не стоит ли на этой клетке чёрная шашка
               && CanMove(whiteShapes,Board[j-2][i+2].getPosition())//проверяем не стоит ли на этой клетке белая шашка
               && CanMove(blackShapes,Board[j-1][i+1].getPosition()) == false)//проверяем можем ли "перепрыгнуть" шашку противника
            {
                canMoveForward = false;
                Board[j-2][i+2].setFillColor(sf::Color::Green);
                window.draw(Board[j-2][i+2]);
            }
        }
        if(canMoveForward){
            if(i+1 <=7 && j+1 <=7){
                if(CanMove(blackShapes,Board[j+1][i+1].getPosition()) && CanMove(whiteShapes,Board[j+1][i+1].getPosition()))
                {
                    Board[j+1][i+1].setFillColor(sf::Color::Green);
                    window.draw(Board[j+1][i+1]);
                }
            }
            if(i-1 >=0 && j+1 <= 7) {
                if(CanMove(blackShapes,Board[j+1][i-1].getPosition()) && CanMove(whiteShapes,Board[j+1][i-1].getPosition())){
                    Board[j + 1][i - 1].setFillColor(sf::Color::Green);
                    window.draw(Board[j + 1][i - 1]);
                }
            }
        }

    }
    else{
        if(j-2>=0 && i + 2 <=7){
            if(CanMove(blackShapes,Board[j-2][i+2].getPosition()) //проверяем не стоит ли на этой клетке чёрная шашка
               && CanMove(whiteShapes,Board[j-2][i+2].getPosition())//проверяем не стоит ли на этой клетке белая шашка
               && CanMove(whiteShapes,Board[j-1][i+1].getPosition()) == false)//проверяем можем ли "перепрыгнуть" шашку противника
            {
                canMoveForward = false;
                Board[j-2][i+2].setFillColor(sf::Color::Green);
                window.draw(Board[j-2][i+2]);
            }
        }
        if(j-2>=0 && i - 2 <=7){
            if(CanMove(blackShapes,Board[j-2][i-2].getPosition()) //проверяем не стоит ли на этой клетке чёрная шашка
               && CanMove(whiteShapes,Board[j-2][i-2].getPosition())//проверяем не стоит ли на этой клетке белая шашка
               && CanMove(whiteShapes,Board[j-1][i-1].getPosition()) == false)//проверяем можем ли "перепрыгнуть" шашку противника
            {
                canMoveForward = false;
                Board[j-2][i-2].setFillColor(sf::Color::Green);
                window.draw(Board[j-2][i-2]);
            }
        }
        if(j+2<=7 && i + 2 <=7){
            if(CanMove(blackShapes,Board[j+2][i+2].getPosition()) //проверяем не стоит ли на этой клетке чёрная шашка
               && CanMove(whiteShapes,Board[j+2][i+2].getPosition())//проверяем не стоит ли на этой клетке белая шашка
               && CanMove(whiteShapes,Board[j+1][i+1].getPosition()) == false)//проверяем можем ли "перепрыгнуть" шашку противника
            {
                canMoveForward = false;
                Board[j+2][i+2].setFillColor(sf::Color::Green);
                window.draw(Board[j+2][i+2]);
            }
        }
        if(j+2 <=7 && i - 2 >=0){
            if(CanMove(blackShapes,Board[j+2][i-2].getPosition()) //проверяем не стоит ли на этой клетке чёрная шашка
               && CanMove(whiteShapes,Board[j+2][i-2].getPosition())//проверяем не стоит ли на этой клетке белая шашка
               && CanMove(whiteShapes,Board[j+1][i-1].getPosition()) == false)//проверяем можем ли "перепрыгнуть" шашку противника
            {
                canMoveForward = false;
                Board[j+2][i-2].setFillColor(sf::Color::Green);
                window.draw(Board[j+2][i-2]);
            }
        }
        if(canMoveForward){
            if(j-1 >=0 && i + 1 <=7){
                if(CanMove(blackShapes,Board[j-1][i+1].getPosition()) && CanMove(whiteShapes,Board[j-1][i+1].getPosition()))
                {
                    Board[j-1][i+1].setFillColor(sf::Color::Green);
                    window.draw(Board[j-1][i+1]);
                }
            }
            if(j-1 >=0 && i-1 >=0){
                if(CanMove(blackShapes,Board[j-1][i-1].getPosition()) && CanMove(whiteShapes,Board[j-1][i-1].getPosition()))
                {
                    Board[j-1][i-1].setFillColor(sf::Color::Green);
                    window.draw(Board[j-1][i-1]);
                }
            }
        }
    }
}

sf::Vector2f GetPositionDeleteFigure(sf::Vector2f newPosition, sf::Vector2f oldPosition){
    return sf::Vector2f(
            (newPosition.x + oldPosition.x)/2,
            (newPosition.y +oldPosition.y)/2
            );
}

struct SelectedShape{
    sf::Color Color;
    int number;
};


struct Command{
    int x;
    int y;
};

std::queue<Command> comands;


std::string uniqueName() {
    srand(time(0));
    auto randchar = []() -> char
    {
        const char charset[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(10,0);
    std::generate_n( str.begin(), 10, randchar );
    return str;
}


void SendHelloServer(Poco::Net::StreamSocket &ss,std::string myID){
    nlohmann::json authJson;
    authJson["from"] = "server";
    authJson["request"] = "auth";

    nlohmann::json clientInfo;
    clientInfo["id"] = myID ;
    clientInfo["username"] = "user";

    authJson["client"] = clientInfo;

    std::string request = authJson.dump();

    ss.sendBytes(request.data(),request.length()+1);
}

void CreateGame(Poco::Net::StreamSocket &ss,std::string myID){
    nlohmann::json authJson;
    authJson["from"] = "server";
    authJson["request"] = "search_game";

    nlohmann::json clientInfo;
    authJson["id"] = myID;
    std::string request = authJson.dump();
    ss.sendBytes(request.data(),request.length()+1);
}

void SendMotion(Poco::Net::StreamSocket &ss,std::string myID,std::string opponentID,int x,int y){
    nlohmann::json gameJson;
    gameJson["from"] = "server";
    gameJson["request"] = "game";
    gameJson["id"] = myID;
    gameJson["opponentID"] = opponentID;
    nlohmann::json motionJson;
    motionJson["x"] = x;
    motionJson["y"] = y;
    gameJson["motion"] = motionJson;
    std::string request = gameJson.dump();
    ss.sendBytes(request.data(),request.length()+1);
}


nlohmann::json ServerMessage(Poco::Net::StreamSocket &ss
)
{
    nlohmann::json json;
    char incommingBuffer[1024];
    int incomingBytes;
    Poco::Timespan timeOut(10,0);
    bool isOpen = true;
    int nBytes = -1;
        try {
            nBytes = ss.receiveBytes(incommingBuffer, sizeof(incommingBuffer));
        }
        catch (Poco::Exception &exc) {
            //Handle your network errors.
            std::cerr << "Network error: " << exc.displayText() << std::endl;
            isOpen = false;
        }

        if (nBytes == 0) {
            std::cout << "Client closes connection!" << std::endl << std::flush;
            isOpen = false;
        } else {
            std::string response(incommingBuffer);
            return nlohmann::json::parse(response);
        }
    return json;
}

struct Game{
    bool isMyMotion;
    sf::Color color;
};


bool EventGame(bool myMotion,sf::RenderWindow &window,
               std::vector<sf::CircleShape> &whiteShapes,
               std::vector<sf::CircleShape> &blackShapes,
               std::vector<std::vector<sf::RectangleShape>> &Board,
               SelectedShape &curentShape,Game &currentGame,int cellSize,int x,int y
               ){
    bool result = false;
    std::pair<bool, int> WhiteFigure = SetOutlineColorForCircleShapes(whiteShapes,x,y,cellSize);
    std::pair<bool, int> BlackFigure = SetOutlineColorForCircleShapes(blackShapes,x,y,cellSize);
    sf::Vector2f position = GetPositionRectagleShape(Board, x,y, cellSize);
    if (WhiteFigure.first && WhiteFigure.second != -1) {
        curentShape.Color = sf::Color::White;
        curentShape.number = WhiteFigure.second;
            whiteShapes[curentShape.number].setOutlineColor(sf::Color::Red);
            DrawBoard(window, Board, cellSize);
            SetFillColorForCells(window, Board, whiteShapes, blackShapes, sf::Color::White, position,
                                 cellSize);

    } else if (BlackFigure.first && BlackFigure.second != -1) {
        curentShape.Color = sf::Color::Yellow;
        curentShape.number = BlackFigure.second;
            blackShapes[curentShape.number].setOutlineColor(sf::Color::Red);
            DrawBoard(window, Board, cellSize);
            SetFillColorForCells(window, Board, whiteShapes, blackShapes, sf::Color::Black, position, cellSize);

    } else {
        if (curentShape.number != -1) {
            if (CanMovedFigure(Board, position)) {
                if (curentShape.Color == sf::Color::White) {
                    sf::Vector2f deletedPosition = GetPositionDeleteFigure(position,
                                                                           whiteShapes[curentShape.number].getPosition());
                    DeleteFigureFromBoard(blackShapes, deletedPosition);
                    whiteShapes[curentShape.number].setPosition(position.x, position.y);
                } else {
                    sf::Vector2f deletedPosition = GetPositionDeleteFigure(position,
                                                                           blackShapes[curentShape.number].getPosition());
                    DeleteFigureFromBoard(whiteShapes, deletedPosition);
                    blackShapes[curentShape.number].setPosition(position.x, position.y);
                }
                curentShape.number = -1;
                result = true;
            }
        }
        DrawBoard(window, Board, cellSize);
    }
    return result;
}


static void ServerMessaging(Poco::Net::StreamSocket &ss,sf::RenderWindow &window,
                            std::vector<sf::CircleShape> &whiteShapes,
                            std::vector<sf::CircleShape> &blackShapes,
                            std::vector<std::vector<sf::RectangleShape>> &Board,
                            SelectedShape &curentShape,int cellSize){
    bool isOpen = true;
    Poco::Timespan timeOut(10,0);
    char incommingBuffer[1000];
    while(isOpen){
        if (ss.poll(timeOut,Poco::Net::Socket::SELECT_READ) == false){
            std::cout << "TIMEOUT!" << std::endl << std::flush;
        }
        else{
            std::cout << "RX EVENT!!! ---> "   << std::flush;
            int nBytes = -1;

            try {
                nBytes = ss.receiveBytes(incommingBuffer, sizeof(incommingBuffer));
            }
            catch (Poco::Exception& exc) {
                //Handle your network errors.
                std::cerr << "Network error: " << exc.displayText() << std::endl;
                isOpen = false;
            }

            if (nBytes==0){
                std::cout << "Client closes connection!" << std::endl << std::flush;
                isOpen = false;
            }
            else{
                std::string command(incommingBuffer);
                nlohmann::json commandJson = nlohmann::json::parse(command);
                if(commandJson["message"] == "game"){
                    Command cmd;
                    cmd.x = commandJson["motion"]["x"];
                    cmd.y = commandJson["motion"]["y"];
                    comands.push(cmd);
                }
            }
        }
    }
}

int main() {

    Poco::Net::StreamSocket ss;
    ss.connect(Poco::Net::SocketAddress("localhost",1234));
    std::string myID = uniqueName();
    std::cout << myID << std::flush;
    SendHelloServer(ss,myID);
    if(ServerMessage(ss)["message"] != "Hello"){
        std::cout << "Connection false" << std::flush;
        return -1;
    }

    CreateGame(ss,myID);
    nlohmann::json startGame = ServerMessage(ss);
    if(startGame["message"] != "game_is_start"){
        std::cout << "Game not start" << std::flush;
        return -1;
    }

    Game currentGame;

    if(startGame["clr"]=="white"){
        currentGame.isMyMotion = true;
        currentGame.color = sf::Color::White;

    }
    else{
        currentGame.isMyMotion = false;
        currentGame.color = sf::Color::Black;
    }
    std::string strGame = startGame.dump();
    int cellSize = 64;

    sf::RenderWindow window(sf::VideoMode(512, 512), startGame["clr"].dump());
    auto t = window.getView();
    t.zoom(0);
    std::vector<std::vector<sf::RectangleShape>> Board(8, std::vector<sf::RectangleShape>(8,sf::RectangleShape()));

    std::vector<sf::CircleShape> whiteShapes(12, sf::CircleShape());
    SetWhiteInitPosition( whiteShapes,0,0,cellSize);

    std::vector<sf::CircleShape> blackShapes(12, sf::CircleShape());
    SetBlackInitPosition(blackShapes,0,0,cellSize);

    SelectedShape curentShape{sf::Color::White,-1};

    bool changeColor = false;
    std::pair<int,int> coordinate;

    window.clear(sf::Color::White);
    DrawBoard(window,Board,cellSize);

    std::thread startMessaging(ServerMessaging,std::ref(ss),std::ref(window),std::ref(whiteShapes),std::ref(blackShapes),std::ref(Board),std::ref(curentShape),cellSize);

    while (window.isOpen()) {
        sf::Event event;

        while (window.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed: {
                    window.close();
                    break;
                }
                case ::sf::Event::MouseButtonReleased: {
                    if (currentGame.isMyMotion) {
                        auto x = event.mouseButton.x;
                        auto y = event.mouseButton.y;
                        std::pair<bool,int> canMotion{false,-1};
                        if(currentGame.color == sf::Color::White){
                           canMotion = SetOutlineColorForCircleShapes(blackShapes,x,y,cellSize);
                        }else{
                            canMotion = SetOutlineColorForCircleShapes(whiteShapes,x,y,cellSize);
                        }
                        if(!canMotion.first){
                            SendMotion(ss, myID, startGame["opponentID"], x, y);
                            currentGame.isMyMotion = !EventGame(currentGame.isMyMotion, window, whiteShapes, blackShapes, Board, curentShape,currentGame,
                                                                cellSize, x, y);
                        }
                        break;
                    }
                }
            }
        }
        if(!comands.empty()){
            Command cmd =  comands.front();
            comands.pop();
            currentGame.isMyMotion = EventGame(currentGame.isMyMotion, window, whiteShapes, blackShapes, Board, curentShape,
                      currentGame,cellSize, cmd.x, cmd.y);
        }


        //DrawBoard(window, Board, cellSize);
        DrawFigureOnBoard(window,whiteShapes);
        DrawFigureOnBoard(window,blackShapes);
        window.display();
    }
   //startMessaging.join();
    return 0;
}
