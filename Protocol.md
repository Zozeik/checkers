# Разработка протокола

Для взаимодействия с сервером необходим протокол обмена информацией. Протокол представляет из себя язык, который одновременно понятен серверу и клиенту. С его помощью клиент отправляет запросы на сервер и получает от него ответы.

В качестве формата передачи данных будет использоваться формат - json. 

## Библиотека nlohmann/json

Данная библиотека является очень удобной и широко используемой для работы с форматом json.
Основные принципы работы с ней будут изложены в ходе дальнейшего изучения материала.

Установить библиотеку можно с помощью команды:
```$xslt
apt-get install nlohman-json3-dev
```

Для использования в проекте достаточно подключить загаловочный файл:
```$xslt
#include <nlohmann/json.hpp>
```

## Взаимодействие с сервером
При подключении к серверу клиент должен сообщить аутентификационные данные. На данном этапе это будет случайно сгенерированное число.
На сервер будет уходить сообщение следующего вида:

```$xslt
{
    "from":"server",
    "request":"auth",
    "client":{
            "id":"сгенерированный номер",
            "username": "имя пользователя"
    }
}
```

С помощью данного сообщения мы сообщаем серверу:
1. Сообщение было адресовано серверу
2. Будет выполнять процедура "auth"
3. Этот запрос отправил клиент с идентификатором id и username

Ниже приведён код на языке C++, который реализует данное сообщение:
```$xslt
    //создаём объект для передачи
    nlohmann::json authJson;
    authJson["from"] = "server";
    authJson["request"] = "auth";
    //создаём вложенный объект в формате json
    nlohmann::json clientInfo;
    clientInfo["id"] = uniqueName();
    clientInfo["username"] = "user";
    //присваиваем полю client созданный json-объект
    authJson["client"] = clientInfo;
    //преобразуем json в строковое представление
    std::string request = authJson.dump();
```

В ответ на данное сообщение сервер должен "поздороваться" с клиентом. Клиенту будет отправлен следующий пакет:
```$xslt
    nlohmann::json clientInfo = json["client"];
    nlohmann::json jsonResponse;
    jsonResponse["from"] = "Client";
    jsonResponse["clientID"] = clientInfo["id"];
    jsonResponse["message"] = "Hello";
    std::string response = jsonResponse.dump();
```

Если клиент получил данное сообщение, то мы можем приступать к методу создания игры между двумя пользователями.

Определим запрос поиска игры следующим образом:

```asm
    //создаём объект для передачи
    nlohmann::json searchJson;
    searchJson["from"] = "server";
    searchJson["request"] = "search_game";
    nlohmann::json clientInfo;
    clientInfo["id"] = uniqueName();
    clientInfo["username"] = "user";
    searchJson["client"] = clientInfo;
    std::string request = authJson.dump()searchJson

```